install mysql for PC
https://dev.mysql.com/downloads/workbench/

install node.js version 12.13.0 (recomended)

open directory ./backend
open terminal
npm install
node server.js

open directory ./frontend
open terminal
npm install
ng serve --port 8081
