const db = require("../models");
const { response } = require("express");
const sequelize = require("sequelize");
const ContactUser = db.PIL_contact_users;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.create = (req, res) => {
  // Validate request
  if (!req.body.nama) {
    res.status(400).send({
      message: "Nama tidak dapat dikosongkan!"
    });
    return;
  }

  // Create a Tutorial
  const contactUser = {
    Nama: req.body.nama,
    Alamat: req.body.alamat,
    Email: req.body.email,
    NoTelepon: req.body.noTelepon,
    InstagramID: req.body.instagramID,
    Sumber: req.body.sumber,
    Status: req.body.status ? req.body.status : false,
    CreateTime: req.body.createTime,
  };

  // Save Tutorial in the database
  ContactUser.create(contactUser)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};

// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {
  const instagramID = req.query.instagramid;
  var condition = instagramID ? { InstagramID: { [Op.like]: `%${instagramID}%` } } : null;

  ContactUser.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Retrieve all Tutorials from the database.
exports.findAllIsContact = (req, res) => {
  const ContactAll = ContactUser.findAll({
    where: { IsContact: 1 }
  })

  const sumberStatWeb = ContactUser.count({where: {Sumber: "web", IsContact: 1}});

  const sumberStatIG = ContactUser.count({where: {Sumber: "ig", IsContact: 1}});

  const statusStatActive = ContactUser.count({where: {Status: true, IsContact: 1}});

  const statusStatNonActive = ContactUser.count({where: {Status: false, IsContact: 1}});

  const countContactByDay = ContactUser.findAll({
    attributes: [
      [sequelize.fn('COUNT', sequelize.col('id')), 'sum'],
      // [sequelize.fn('DAY', sequelize.col('ContactCreated')), 'date'], alternative
      [sequelize.fn('date_format', sequelize.col('ContactCreated'), '%Y-%m-%d'), 'date'],
    ],
    where: {
      IsContact: 1,
    },
    group: ['date'],
    order:  ['ContactCreated']
  });

  Promise
    .all([ContactAll, sumberStatWeb, sumberStatIG, statusStatActive, statusStatNonActive, countContactByDay])
    .then(response => {
      res.send({
        contacts: response[0],
        sumberStatWeb: response[1],
        sumberStatIG: response[2],
        statusStatActive: response[3],
        statusStatNonActive: response[4],
        countContactStat: response[5]
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  ContactUser.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Tutorial with id=" + id
      });
    });
};

// get contact using filter
exports.findContactParams = (req, res) => {
  const id = req.params.id;
  const param = JSON.parse(req.params.object_query);
  let where = {};
  if (!!param.dateFrom && !!param.dateUntil) {
    where = {
      Sumber: param.Sumber,
      ContactCreated: {
        [Op.between]: [param.dateFrom, param.dateUntil]
      },
      Status: {
        [Op.or]: [param.Status, !param.StatusInactive]
      },
      Sumber: {
        [Op.or]: [param.SumberWeb === true ? 'web' : '', param.SumberIG === true ? 'ig' : '']
      },
      IsContact: 1
    };
  } else {
    where = {
      Sumber: param.Sumber,
      Status: {
        [Op.or]: [param.Status, !param.StatusInactive]
      },
      Sumber: {
        [Op.or]: [param.SumberWeb === true ? 'web' : '', param.SumberIG === true ? 'ig' : '']
      },
      IsContact: 1
    };
  }
  const contactFilter = ContactUser.findAll({
    where
  });

  const sumberStatWeb = ContactUser.count({where: {Sumber: "web"}});

  const sumberStatIG = ContactUser.count({where: {Sumber: "ig"}});

  const statusStatActive = ContactUser.count({where: {Status: true}});

  const statusStatNonActive = ContactUser.count({where: {Status: false}});

  Promise
    .all([contactFilter, sumberStatWeb, sumberStatIG, statusStatActive, statusStatNonActive])
    .then(response => {
      res.send({
        contacts: response[0],
        sumberStatWeb: response[1],
        sumberStatIG: response[2],
        statusStatActive: response[3],
        statusStatNonActive: response[4],
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  console.log(req.body);
  ContactUser.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Tutorial was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Tutorial with id=" + id
      });
    });
};

// Delete a Tutorial with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  ContactUser.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Tutorial was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Tutorial with id=" + id
      });
    });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
  ContactUser.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Tutorials were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all tutorials."
      });
    });
};

// find all status active Tutorial
exports.findAllActiveStatus = (req, res) => {
  ContactUser.findAll({ where: { status: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};
