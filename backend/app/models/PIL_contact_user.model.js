module.exports = (sequelize, Sequelize) => {
  const PIL_contact_user = sequelize.define("PIL_contact_user", {
    Nama: {
      type: Sequelize.STRING
    },
    Email: {
      type: Sequelize.STRING
    },
    Alamat: {
      type: Sequelize.STRING
    },
    NoTelepon: {
      type: Sequelize.STRING
    },
    InstagramID: {
      type: Sequelize.STRING
    },
    Sumber: {
      type: Sequelize.STRING
    },
    Status: {
      type: Sequelize.BOOLEAN
    },
    IsContact: {
      type: Sequelize.BOOLEAN
    },
    ContactCreated: {
      type: Sequelize.DATE
    },
    CreateTime: {
      type: Sequelize.DATE
    }
  });

  return PIL_contact_user;
};
