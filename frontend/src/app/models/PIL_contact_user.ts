export class PIL_contact_user {
    nama: string;
    alamat: string;
    email: string;
    noTelepon: string;
    instagramID: string;
    status: boolean;
    sumber: string;
    isContact: boolean;
    contactCreated: string;
    createTime: string;
}