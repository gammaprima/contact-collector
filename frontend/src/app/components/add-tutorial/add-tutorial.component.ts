import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/services/tutorial.service';
import { ContactUserService } from 'src/app/services/contact-user.service';
import { PIL_contact_user } from 'src/app/models/PIL_contact_user';

@Component({
  selector: 'app-add-tutorial',
  templateUrl: './add-tutorial.component.html',
  styleUrls: ['./add-tutorial.component.css']
})
export class AddTutorialComponent implements OnInit {
  tutorial = {
    title: '',
    description: '',
    published: false
  };
  contactUser: PIL_contact_user = {
    nama: '',
    alamat: '',
    email: '',
    noTelepon: '',
    instagramID: '',
    status: false,
    sumber: 'web',
    isContact: false,
    contactCreated: '',
    createTime: '',
  };
  submitted = false;

  constructor(private tutorialService: TutorialService,
              private contactUserService: ContactUserService) { }

  ngOnInit(): void {
  }

  saveContact(): void {
    const data = {
      nama: this.contactUser.nama,
      alamat: this.contactUser.alamat,
      email: this.contactUser.email,
      noTelepon: this.contactUser.noTelepon,
      instagramID: this.contactUser.instagramID,
      status: this.contactUser.status,
      sumber: this.contactUser.sumber,
      isContact: this.contactUser.isContact,
      contactCreated: this.contactUser.contactCreated,
      createTime: this.contactUser.createTime,
    };
    console.log(data);

    this.contactUserService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newTutorial(): void {
    this.submitted = false;
    this.contactUser = {
      nama: '',
      alamat: '',
      email: '',
      noTelepon: '',
      instagramID: '',
      status: false,
      sumber: 'web',
      isContact: false,
      contactCreated: '',
      createTime: '',
    };
  }

}
