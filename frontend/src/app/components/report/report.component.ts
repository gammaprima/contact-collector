import { Component, OnInit } from '@angular/core';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { MultiDataSet, Color, Label } from 'ng2-charts';
import { ContactUserService } from 'src/app/services/contact-user.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  contacts: any = null;
  selectedSumber: string = "web";
  sumberWeb: boolean = true;
  sumberIG: boolean = true;
  selectedDate: any;
  selectedDateUntil: any;
  activeStatus: boolean = true;
  nonActiveStatus: boolean = true;

  datePickerConfig: any;
  // Doughnut
  public activeChartLabels: Label[] = ['Jumlah Kontak yang Aktif', 'Jumlah Kontak yang Tidak Aktif'];
  public activeChartData: MultiDataSet = [
    [0, 0],
  ];

  public sumberChartLabels: Label[] = ['Sumber Web', 'Sumber Instagram'];
  public sumberChartData: MultiDataSet = [
    [0, 0],
  ];
  
  public doughnutChartType: ChartType = 'doughnut';
  // line
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Peningkatan Jumlah Kontak per Hari' },
  ];
  public lineChartLabels: Label[] = [];
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  constructor(private contactUserService: ContactUserService,) { }

  ngOnInit(): void {
    this.retrieveContacts();
    this.datePickerConfig = {
      disableKeypress: true,
      format: ' YYYY-MM-DD hh:mm:ss '
    }
  }

  retrieveContacts(): void {
    this.contactUserService.getAllIsContact()
      .subscribe(
        data => {
          this.contacts = data.contacts;
          console.log(data);
          this.activeChartData = [data.statusStatActive, data.statusStatNonActive];
          this.sumberChartData = [data.sumberStatWeb, data.sumberStatIG];
          this.setLineChartCountContact(data.countContactStat);
        },
        error => {
          console.log(error);
        });
  }

  setLineChartCountContact(countContactStat) {
    let sum = [];
    let dates = [];
    countContactStat.forEach(contact => {
      try {
        sum.push(contact.sum);
        dates.push(contact.date);
      } catch {
        sum = [contact.sum[0]];
        dates = [contact.date[0]];
      }
    });
    console.log(sum);
    console.log(dates);
    this.lineChartData = [
      { data: sum, label: 'Peningkatan Jumlah Kontak per Hari' },
    ];
    this.lineChartLabels = dates;
  }

  showFilter() {
    const dateFrom = !!this.selectedDate ? this.selectedDate._d.toISOString().slice(0, 19).replace('T', ' ') : '';
    const dateUntil = !!this.selectedDateUntil ? this.selectedDateUntil._d.toISOString().slice(0, 19).replace('T', ' ') : '';
    console.log(this.activeStatus);
    console.log(this.nonActiveStatus);
    const params = JSON.stringify({
      SumberWeb: this.sumberWeb,
      SumberIG: this.sumberIG,
      dateFrom,
      dateUntil,
      Status: this.activeStatus,
      StatusInactive: this.nonActiveStatus
    })
    this.contactUserService.getContactFilter(params)
      .subscribe(
        data => {
          this.contacts = data.contacts;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  deleteContact(id, index) {
    this.contactUserService.update(id, {ContactCreated: null, IsContact: 0})
      .subscribe(
        response => {
          console.log(response);
          this.contacts.splice(index, 1);
        },
        error => {
          console.log(error);
        });
  }
}
