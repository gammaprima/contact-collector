import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/services/tutorial.service';
import { ContactUserService } from 'src/app/services/contact-user.service';
import { PIL_contact_user } from 'src/app/models/PIL_contact_user';

@Component({
  selector: 'app-follower-list',
  templateUrl: './follower-list.component.html',
  styleUrls: ['./follower-list.component.css']
})
export class FollowerListComponent implements OnInit {

  tutorials: any;
  currentTutorial: PIL_contact_user = null;
  currentContact: any = null;
  currentIndex = -1;
  title = '';
  bgColorList = ["bg-danger", "bg-primary", "bg-success", "bg-warning", "bg-dark-blue"]

  constructor(private tutorialService: TutorialService,
              private contactUserService: ContactUserService) { }

  ngOnInit(): void {
    this.retrieveTutorials();
  }

  retrieveTutorials(): void {
    this.contactUserService.getAll()
      .subscribe(
        data => {
          this.tutorials = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveTutorials();
    this.currentContact = null;
    this.currentIndex = -1;
  }

  setActiveTutorial(contact, index): void {
    this.currentContact = contact;
    this.currentIndex = index;
  }

  removeAllTutorials(): void {
    this.tutorialService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchInstagramID(): void {
    this.contactUserService.findByInstagramID(this.title)
      .subscribe(
        data => {
          this.tutorials = data;
          this.currentContact = null;
          this.currentIndex = -1;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  searchInstagramIDWhenNull() {
    if (!this.title) {
      this.searchInstagramID();
    }
  }

  addToContact(id) {
    this.currentContact.IsContact = 1;
    const date = new Date().toISOString().slice(0, 19).replace('T', ' ');
    this.contactUserService.update(id, {ContactCreated: date, IsContact: 1})
      .subscribe(
        response => {
          console.log(response);
        },
        error => {
          this.currentContact.IsContact = 0;
          console.log(error);
        });
  }
}
